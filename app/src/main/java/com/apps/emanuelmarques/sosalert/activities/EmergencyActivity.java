/*
 * Developed by Emanuel Marques on 31-07-2017 14:30
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified on 26-07-2017 14:15
 */

package com.apps.emanuelmarques.sosalert.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.apps.emanuelmarques.sosalert.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import emanuelmarques.sosalert.analytics.AppAnalytics;
import emanuelmarques.sosalert.communication.SMSSender;
import emanuelmarques.sosalert.location.LocationHelper;

public class EmergencyActivity extends AppCompatActivity
        implements OnMapReadyCallback {

    private SMSSender smsSender;
    private GoogleMap googleMap;
    private LocationManager locManager;
    private static final String TAG = MainActivity.class.getSimpleName();
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AppAnalytics application = (AppAnalytics) getApplication();
        mTracker = application.getDefaultTracker();

        prepareButtons();
        prepareMap();
    }

    private void prepareMap() {
        LocationListener loc = new EmergencyActivity.mLocListener();

        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, loc);

        android.app.FragmentManager fm = getFragmentManager();
        android.app.FragmentTransaction ft = fm.beginTransaction();
        MapFragment mf = new MapFragment();
        ft.add(R.id.mapLayout, mf);
        ft.commit();
        mf.getMapAsync(this);
    }

    private void prepareButtons() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Ending state of Emergency", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                endEmergencyState();
            }
        });

        //location update
        Button updateBtn = (Button) findViewById(R.id.btnUpdateLoc);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    smsSender = new SMSSender(EmergencyActivity.this);
                    smsSender.sendSMS(getResources().getString(R.string.location_update)
                            + LocationHelper.getLocationMapsUrl(MainActivity.currentLatitude, MainActivity.currentLongitude));
                }
            }
        });

        //fire
        Button fireBtn = (Button) findViewById(R.id.fireUpdateBtn);
        fireBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    smsSender = new SMSSender(EmergencyActivity.this);
                    smsSender.sendSMS(getResources().getString(R.string.fire_update));
                    smsSender.sendSMS(getResources().getString(R.string.location_update)
                            + LocationHelper.getLocationMapsUrl(MainActivity.currentLatitude, MainActivity.currentLongitude));
                }
            }
        });

        Button robberyBtn = (Button) findViewById(R.id.robberyUpdateBtn);
        robberyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    smsSender = new SMSSender(EmergencyActivity.this);
                    smsSender.sendSMS(getResources().getString(R.string.robbery_update));
                    smsSender.sendSMS(getResources().getString(R.string.location_update)
                            + LocationHelper.getLocationMapsUrl(MainActivity.currentLatitude, MainActivity.currentLongitude));
                }
            }
        });

        Button kidnappingBtn = (Button) findViewById(R.id.kidnappingUpdateBtn);
        kidnappingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    smsSender = new SMSSender(EmergencyActivity.this);
                    smsSender.sendSMS(getResources().getString(R.string.kidnapping_update));
                    smsSender.sendSMS(getResources().getString(R.string.location_update)
                            + LocationHelper.getLocationMapsUrl(MainActivity.currentLatitude, MainActivity.currentLongitude));
                }
            }
        });

        Button lostBtn = (Button) findViewById(R.id.kidnappingUpdateBtn);
        lostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    smsSender = new SMSSender(EmergencyActivity.this);
                    smsSender.sendSMS(getResources().getString(R.string.lost_update));
                    smsSender.sendSMS(getResources().getString(R.string.location_update)
                            + LocationHelper.getLocationMapsUrl(MainActivity.currentLatitude, MainActivity.currentLongitude));
                }
            }
        });

        Button carBtn = (Button) findViewById(R.id.carBrokeUpdateBtn);
        carBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    smsSender = new SMSSender(EmergencyActivity.this);
                    smsSender.sendSMS(getResources().getString(R.string.car_update));
                    smsSender.sendSMS(getResources().getString(R.string.location_update)
                            + LocationHelper.getLocationMapsUrl(MainActivity.currentLatitude, MainActivity.currentLongitude));
                }
            }
        });

        Button medicalBtn = (Button) findViewById(R.id.medicalUpdateBtn);
        medicalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    smsSender = new SMSSender(EmergencyActivity.this);
                    smsSender.sendSMS(getResources().getString(R.string.medical_update));
                    smsSender.sendSMS(getResources().getString(R.string.location_update)
                            + LocationHelper.getLocationMapsUrl(MainActivity.currentLatitude, MainActivity.currentLongitude));
                }
            }
        });
    }

    private void confirmStopEmergencyState() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Do you want to stop de emergency state?");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                endEmergencyState();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void endEmergencyState() {
        if (ActivityCompat.checkSelfPermission(EmergencyActivity.this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
            smsSender = new SMSSender(EmergencyActivity.this);
            smsSender.sendSMS(getResources().getString(R.string.emergency_end)
                    + LocationHelper.getLocationMapsUrl(MainActivity.currentLatitude, MainActivity.currentLongitude));
        }
        Intent emergencyState = new Intent(EmergencyActivity.this, MainActivity.class);
        startActivity(emergencyState);
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i(TAG, "Setting screen name: " + TAG);
        mTracker.setScreenName("EmergencyActivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.enableAutoActivityTracking(true);
    }

    @Override
    public void onBackPressed() {
        confirmStopEmergencyState();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        map.setMyLocationEnabled(true);
        map.setTrafficEnabled(true);
        map.setIndoorEnabled(true);
        map.setBuildingsEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(false);

        googleMap = map;
        List<String> providers = locManager.getProviders(true);

        Location l = null;
        int i;
        for (i = providers.size() - 1; i >= 0; i--) {
            l = locManager.getLastKnownLocation(providers.get(i));
            if (l != null) break;
        }

        if (l != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(locManager.getLastKnownLocation(providers.get(i)).getLatitude(), locManager.getLastKnownLocation(providers.get(i)).getLongitude()), 17);
            googleMap.animateCamera(cameraUpdate);
        }
    }

    class mLocListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17);
            googleMap.animateCamera(cameraUpdate);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }
}
