/*
 * Developed by Emanuel Marques on 31-07-2017 14:30
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified on 28-07-2017 9:13
 */

package com.apps.emanuelmarques.sosalert.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.apps.emanuelmarques.sosalert.R;

import emanuelmarques.sosalert.analytics.AppAnalytics;
import emanuelmarques.sosalert.entities.Contact;
import emanuelmarques.sosalert.repository.ContactRepository;

public class ContactDetail extends AppCompatActivity implements android.view.View.OnClickListener {

    Button btnSave, btnDelete;
    EditText editTextName;
    EditText editTextNumber;
    CheckBox editEnabled;
    Toolbar toolbar;
    private int _Contact_Id = 0;

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);

        AppAnalytics application = (AppAnalytics) getApplication();

        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);

        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextNumber = (EditText) findViewById(R.id.editTextEmail);
        editEnabled = (CheckBox) findViewById(R.id.checkboxEnabled);

        btnSave.setOnClickListener(this);
        btnDelete.setOnClickListener(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.edit_contact);

        _Contact_Id = 0;
        Intent intent = getIntent();
        _Contact_Id = intent.getIntExtra("contact_Id", 0);
        ContactRepository repo = new ContactRepository(this);
        Contact contact = new Contact();
        contact = repo.getContactById(_Contact_Id);

        editEnabled.setChecked(contact.IsEnabled);
        editTextName.setText(contact.Name);
        editTextNumber.setText(contact.Number);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.btnSave)) {
            if (editTextNumber.getText().toString() == null ||
                    editTextNumber.getText().toString().trim().equals("") ||
                    editTextName.getText().toString() == null ||
                    editTextName.getText().toString().trim().equals("")) {
                Toast.makeText(this, "Name and number are mandatory!", Toast.LENGTH_LONG).show();
            } else {
                ContactRepository repo = new ContactRepository(this);
                Contact contact = new Contact();
                contact.IsEnabled = editEnabled.isChecked();
                contact.Number = editTextNumber.getText().toString();
                contact.Name = editTextName.getText().toString();
                contact.ContactId = _Contact_Id;

                if (_Contact_Id == 0) {
                    _Contact_Id = repo.insert(contact);

                    Toast.makeText(this, "New Contact Insert", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    repo.update(contact);
                    Toast.makeText(this, "Contact Record updated", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        } else if (view == findViewById(R.id.btnDelete)) {
            ContactRepository repo = new ContactRepository(this);
            repo.delete(_Contact_Id);
            Toast.makeText(this, "Contact Record Deleted", Toast.LENGTH_SHORT);
            finish();
        }
    }
}
