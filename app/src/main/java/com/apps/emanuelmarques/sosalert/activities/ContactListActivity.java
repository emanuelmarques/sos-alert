/*
 * Developed by Emanuel Marques on 31-07-2017 14:30
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified on 27-07-2017 0:24
 */

package com.apps.emanuelmarques.sosalert.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.emanuelmarques.sosalert.R;

import java.util.ArrayList;
import java.util.HashMap;

import emanuelmarques.sosalert.analytics.AppAnalytics;
import emanuelmarques.sosalert.repository.ContactRepository;

public class ContactListActivity extends AppCompatActivity implements android.view.View.OnClickListener {

    FloatingActionButton btnAdd;
    TextView contact_Id;

    Toolbar toolbar;

    private static final String TAG = MainActivity.class.getSimpleName();
    @Override
    public void onClick(View view) {
        if (view == findViewById(R.id.addContactFab)) {
            Intent intent = new Intent(this, ContactDetail.class);
            intent.putExtra("contactId", 0);
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        btnAdd = (FloatingActionButton) findViewById(R.id.addContactFab);
        btnAdd.setOnClickListener(ContactListActivity.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.contacts);
        loadContacts();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadContacts();
    }

    private void loadContacts() {
        ContactRepository repo = new ContactRepository(this);

        ArrayList<HashMap<String, String>> contactList = repo.getStudentList();
        if (contactList.size() != 0) {
            ListView lv = (ListView) findViewById(R.id.list);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    contact_Id = (TextView) view.findViewById(R.id.contact_Id);
                    String contactId = contact_Id.getText().toString();
                    Intent objIndent = new Intent(getApplicationContext(), ContactDetail.class);
                    objIndent.putExtra("contact_Id", Integer.parseInt(contactId));
                    startActivity(objIndent);
                }
            });
            ListAdapter adapter = new SimpleAdapter(
                    ContactListActivity.this,
                    contactList,
                    R.layout.content_contact_entry,
                    new String[]{"id", "name", "number"},
                    new int[]{R.id.contact_Id, R.id.contact_name, R.id.contact_number});
            lv.setAdapter(adapter);
        } else {
            Toast.makeText(this, "No Contacts", Toast.LENGTH_SHORT).show();
        }
    }

}
