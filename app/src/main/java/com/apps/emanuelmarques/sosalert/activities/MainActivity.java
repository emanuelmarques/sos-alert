/*
 * Developed by Emanuel Marques on 31-07-2017 14:30
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified on 27-07-2017 23:42
 */

package com.apps.emanuelmarques.sosalert.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.emanuelmarques.sosalert.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.vision.text.Text;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import emanuelmarques.sosalert.analytics.AppAnalytics;
import emanuelmarques.sosalert.communication.SMSSender;
import emanuelmarques.sosalert.helpers.MathHelpers;
import emanuelmarques.sosalert.location.LocationHelper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {

    public static TextView latitudeView;
    public static TextView longitudeView;
    public static TextView latitude;
    public static TextView longitude;

    public static double currentLatitude;
    public static double currentLongitude;

    private TextView cityView;
    private GoogleMap googleMap;
    private LocationManager locManager;
    private SMSSender smsSender;
    private Tracker mTracker;

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppAnalytics application = (AppAnalytics) getApplication();
        mTracker = application.getDefaultTracker();

        checkPermissions();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, R.string.sos_init, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    smsSender = new SMSSender(MainActivity.this);
                    smsSender.sendSMS(getResources().getString(R.string.emergency_start)
                            + LocationHelper.getLocationMapsUrl(MainActivity.currentLatitude, MainActivity.currentLongitude));

                }

                Intent emergencyState = new Intent(MainActivity.this, EmergencyActivity.class);
                startActivity(emergencyState);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        latitudeView = (TextView) findViewById(R.id.textLatitude);
        longitudeView = (TextView) findViewById(R.id.textLongitude);
        latitude = (TextView) findViewById(R.id.latitude);
        longitude = (TextView) findViewById(R.id.longitude);

        cityView = (TextView) findViewById(R.id.textCity);
        LocationListener loc = new mLocListener();

        latitudeView.setText("Latitude: ");
        longitudeView.setText("Longitude: ");

        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, loc);

        android.app.FragmentManager fm = getFragmentManager();
        android.app.FragmentTransaction ft = fm.beginTransaction();
        MapFragment mf = new MapFragment();
        ft.add(R.id.mapLayout, mf);
        ft.commit();
        mf.getMapAsync(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i(TAG, "Setting screen name: " + TAG);
        mTracker.setScreenName("MainActivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.enableAutoActivityTracking(true);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        map.setMyLocationEnabled(true);
        map.setTrafficEnabled(true);
        map.setIndoorEnabled(true);
        map.setBuildingsEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(false);

        googleMap = map;
        List<String> providers = locManager.getProviders(true);

        Location l = null;
        int i;
        for (i = providers.size() - 1; i >= 0; i--) {
            l = locManager.getLastKnownLocation(providers.get(i));
            if (l != null) break;
        }

        if (l != null) {
            latitude.setText(String.valueOf(MathHelpers.round(locManager.getLastKnownLocation(providers.get(i)).getLatitude(), 5)));
            longitude.setText(String.valueOf(MathHelpers.round(locManager.getLastKnownLocation(providers.get(i)).getLongitude(), 5)));

            currentLatitude = locManager.getLastKnownLocation(providers.get(i)).getLatitude();
            currentLongitude = locManager.getLastKnownLocation(providers.get(i)).getLongitude();

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                    new LatLng(locManager.getLastKnownLocation(providers.get(i))
                            .getLatitude(), locManager.getLastKnownLocation(providers.get(i))
                            .getLongitude()), 17);
            googleMap.animateCamera(cameraUpdate);
        }
    }

    class mLocListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            latitude.setText(String.valueOf(MathHelpers.round(location.getLatitude(), 5)));
            longitude.setText(String.valueOf(MathHelpers.round(location.getLongitude(), 5)));

            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17);
            googleMap.animateCamera(cameraUpdate);
            String cityName = null;
            Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(location.getLatitude(),
                        location.getLongitude(), 1);
                if (addresses.size() > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    cityName = addresses.get(0).getLocality();
                    if (cityName == null) {
                        cityName = addresses.get(0).getAddressLine(0);
                    }
                    cityView.setText(cityName);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("plain/text");
            intent.putExtra(Intent.EXTRA_EMAIL,new String[]{"emanuelsmarques@outlook.pt"});
            intent.putExtra(Intent.EXTRA_SUBJECT, "SOS Alert problem report");
            Intent mailer = Intent.createChooser(intent, getResources().getString(R.string.send_email));
            startActivity(mailer);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.contacts) {
            Intent contactListIntent = new Intent(MainActivity.this, ContactListActivity.class);
            startActivity(contactListIntent);
        } else if (id == R.id.report_problem) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("plain/text");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"emanuelsmarques@outlook.pt"});
            intent.putExtra(Intent.EXTRA_SUBJECT, "SOS Alert problem report");
            Intent mailer = Intent.createChooser(intent, getResources().getString(R.string.send_email));
            startActivity(mailer);
        }
//        } else if (id == R.id.about) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions(){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED){
            }else{
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.INTERNET,
                                Manifest.permission.ACCESS_NETWORK_STATE,
                                Manifest.permission.READ_PHONE_STATE},
                        1);
            }
        }else{
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                finish();
                startActivity(getIntent());
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
