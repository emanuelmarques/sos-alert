/*
 * Developed by Emanuel Marques on 31-07-2017 14:29
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified on 17-07-2017 14:47
 */

package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import emanuelmarques.sosalert.entities.Contact;

public class DatabaseHelper extends SQLiteOpenHelper {
    //version number to upgrade database version
    //each time if you Add, Edit table, you need to change the
    //version number.
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "sos-alert.db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here

        String CREATE_TABLE_CONTACT = "CREATE TABLE " + Contact.TABLE + "("
                + Contact.KEY_ContactID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + Contact.KEY_Name + " TEXT, "
                + Contact.KEY_Number + " TEXT, "
                + Contact.KEY_IsEnabled + " INTEGER )";

        db.execSQL(CREATE_TABLE_CONTACT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS " + Contact.TABLE);

        // Create tables again
        onCreate(db);

    }
}