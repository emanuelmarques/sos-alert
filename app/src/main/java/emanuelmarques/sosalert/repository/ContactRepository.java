package emanuelmarques.sosalert.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import database.DatabaseHelper;
import emanuelmarques.sosalert.entities.Contact;

public class ContactRepository {
    private DatabaseHelper dbHelper;

    public ContactRepository(Context context) {

        dbHelper = new DatabaseHelper(context);
    }

    public int insert(Contact contact) {

        //Open connection to write data
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Contact.KEY_Name, contact.Name);
        values.put(Contact.KEY_Number, contact.Number);
        values.put(Contact.KEY_IsEnabled, contact.IsEnabled);

        // Inserting Row
        long contact_Id = db.insert(Contact.TABLE, null, values);
        db.close(); // Closing database connection
        return (int) contact_Id;
    }

    public void delete(int contact_Id) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // It's a good practice to use parameter ?, instead of concatenate string
        db.delete(Contact.TABLE, Contact.KEY_ContactID + "= ?", new String[]{String.valueOf(contact_Id)});
        db.close(); // Closing database connection
    }

    public void update(Contact contact) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Contact.KEY_Name, contact.Name);
        values.put(Contact.KEY_Number, contact.Number);
        values.put(Contact.KEY_IsEnabled, contact.IsEnabled);

        // It's a good practice to use parameter ?, instead of concatenate string
        db.update(Contact.TABLE, values, Contact.KEY_ContactID + "= ?", new String[]{String.valueOf(contact.ContactId)});
        db.close(); // Closing database connection
    }

    public ArrayList<HashMap<String, String>> getStudentList() {
        //Open connection to read only
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT  " +
                Contact.KEY_ContactID + "," +
                Contact.KEY_Name + "," +
                Contact.KEY_Number + "," +
                Contact.KEY_IsEnabled +
                " FROM " + Contact.TABLE;

        //Contact Contact = new Contact();
        ArrayList<HashMap<String, String>> contactList = new ArrayList<HashMap<String, String>>();

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> contact = new HashMap<String, String>();
                contact.put("id", cursor.getString(cursor.getColumnIndex(Contact.KEY_ContactID)));
                contact.put("name", cursor.getString(cursor.getColumnIndex(Contact.KEY_Name)));
                contact.put("number", cursor.getString(cursor.getColumnIndex(Contact.KEY_Number)));
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return contactList;
    }

    public List<String> getNumbersToSend() {
        //Open connection to read only
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT  " +
                Contact.KEY_Number +
                " FROM " + Contact.TABLE +
                " WHERE " + Contact.KEY_IsEnabled + " = 1";

        //Contact Contact = new Contact();
        List<String> contactList = new ArrayList<String>();

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list

        if (cursor.moveToFirst()) {
            do {
                contactList.add(cursor.getString(cursor.getColumnIndex(Contact.KEY_Number)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return contactList;
    }

    public Contact getContactById(int Id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT  " +
                Contact.KEY_ContactID + "," +
                Contact.KEY_Name + "," +
                Contact.KEY_Number + "," +
                Contact.KEY_IsEnabled +
                " FROM " + Contact.TABLE
                + " WHERE " +
                Contact.KEY_ContactID + "=?";// It's a good practice to use parameter ?, instead of concatenate string

        int iCount = 0;
        Contact contact = new Contact();

        Cursor cursor = db.rawQuery(selectQuery, new String[]{String.valueOf(Id)});

        if (cursor.moveToFirst()) {
            do {
                contact.ContactId = cursor.getInt(cursor.getColumnIndex(Contact.KEY_ContactID));
                contact.Name = cursor.getString(cursor.getColumnIndex(Contact.KEY_Name));
                contact.Number = cursor.getString(cursor.getColumnIndex(Contact.KEY_Number));
                contact.IsEnabled = cursor.getInt(cursor.getColumnIndex(Contact.KEY_IsEnabled)) == 1;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return contact;
    }

}