package emanuelmarques.sosalert.helpers;

/**
 * Created by emanuel.marques on 25-07-2017.
 */

public class MathHelpers {
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
