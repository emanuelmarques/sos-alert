package emanuelmarques.sosalert.helpers;

/**
 * Created by emanuel.marques on 26-07-2017.
 */

public class PermissionRequestCodes {
    public static int STORAGE_PERMISSION_REQUEST = 0;
    public static int SMS_PERMISSION_REQUEST = 1;
    public static int LOCATION_PERMISSION_REQUEST = 2;

}
