package emanuelmarques.sosalert.location;

/**
 * Created by emanuel.marques on 24-07-2017.
 */

public class LocationHelper {

    public static String getLocationMapsUrl(double lat, double lon) {
        return "http://maps.google.com/maps?q=" + lat + "," + lon + "&iwloc=A";
    }
}
