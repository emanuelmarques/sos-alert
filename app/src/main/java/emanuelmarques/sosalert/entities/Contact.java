/*
 * Developed by Emanuel Marques on 31-07-2017 14:31
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified on 17-07-2017 12:02
 */

package emanuelmarques.sosalert.entities;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * Created by emanuel.marques on 17-07-2017.
 */

public class Contact {

    // Labels table name
    public static final String TABLE = "Contact";

    // Labels Table Columns names
    public static final String KEY_ContactID = "ContactId";
    public static final String KEY_Name = "Name";
    public static final String KEY_Number = "Number";
    public static final String KEY_IsEnabled = "IsEnabled";

    public int ContactId;
    public String Name;
    public String Number;
    public boolean IsEnabled;

}
