/*
 * Developed by Emanuel Marques on 31-07-2017 14:30
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified on 26-07-2017 9:29
 */

package emanuelmarques.sosalert.communication;

import android.content.Context;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.apps.emanuelmarques.sosalert.R;
import com.apps.emanuelmarques.sosalert.activities.MainActivity;

import java.util.List;

import emanuelmarques.sosalert.repository.ContactRepository;

/**
 * Created by Emanuel Marques on 08/08/2016.
 */
public class SMSSender {
    private SmsManager smsManager = SmsManager.getDefault();

    private ContactRepository contactRepository;
    private Context context;
    public SMSSender(Context context) {
        this.context = context;
        contactRepository = new ContactRepository(context);
    }

    public void sendSMS(String message) {

        List<String> numbers = contactRepository.getNumbersToSend();

        if (!numbers.isEmpty()){
            for (String number : numbers) {
                smsManager.sendTextMessage(number, null, message, null, null);
            }
            Toast.makeText(context, context.getResources().getString(R.string.sms_sent), Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(context, context.getResources().getString(R.string.no_numbers_available),Toast.LENGTH_LONG).show();
        }

    }
}
